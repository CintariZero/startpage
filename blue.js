function display(msg)
{
	document.getElementById('help').innerHTML = msg;
}
function clear_help()
{
	document.getElementById('help').innerHTML = "";
}

function requires(a)
{
	var b = 'e_'+a;
	var elem = document.getElementById(a);
	if(elem.value == "")
	{
		document.getElementById(b).innerHTML = "This field is required.";
		elem.className = "fielderror";
	}
	else
	{
		document.getElementById(b).innerHTML = "";
		elem.className = "fieldgood";
	}
}

function validation()
{
	var valid = true;

	var charExp = /^[a-zA-Z]+$/;
	var  atExp= /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/; // long but match all valid emails, apparently
	var passExp= /[\S\s]{6,}/;
	
	elem = document.getElementById('email');
	if(!elem.value.match(atExp))
	{
		valid = false;
		document.getElementById('e_email').innerHTML = "Not a valid email address";
		elem.className = "fielderror";
	}
	elem = document.getElementById('name');
	if(!elem.value.match(charExp))
	{
		valid = false;
		document.getElementById('e_name').innerHTML = "The name field must begin with a letter";
		elem.className = "fielderror";
	}
	elem = document.getElementById('password');
	if(!elem.value.match(passExp))
	{
		valid = false;
		document.getElementById('e_password').innerHTML = "Password must be at least 6 characters";
		elem.className = "fielderror";
	}
	var confirm = document.getElementById('retype');
	if(!elem.value.match(confirm.value))
	{
		valid = false;
		document.getElementById('e_retype').innerHTML = "Must match password field exactly";
		elem.className = "fielderror";
	}
	if(valid)
	{
		document.getElementById("testform").submit();
	}
	return valid;
}
