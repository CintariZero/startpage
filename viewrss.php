<?php

error_reporting(E_ALL);
function viewrss($url)
{
	
	$rss = simplexml_load_file($url);
	$feed = "";
	if($rss)
	{
		$feed = '<h2>'.$rss->channel->title.'</h2>';
		$items = $rss->channel->item;
		foreach($items as $item)
		{
			$title = $item->title;
			$link = $item->link;
			$description = $item->description;
			$feed .= '<h3><a href="'.$link.'">'.$title.'</a></h3>';
			$feed .= '<p>'.$description.'</p>';
		}
	}
	return $feed;
}
#echo viewrss("http://feeds.bbci.co.uk/news/rss.xml");
?>
